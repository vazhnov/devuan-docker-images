# syntax=docker/dockerfile:1
# SPDX-License-Identifier: MIT

# Normally, this file starts by GitLab pipeline, see https://gitlab.com/vazhnov/devuan-docker-images/-/blob/main/.gitlab-ci.yml
# Preparations: run locally in fresh Devuan:
# cp -pv /usr/share/keyrings/devuan-archive-keyring.gpg .
# cp -pv /usr/share/debootstrap/scripts/ceres ./debootstrap-scripts-ceres

FROM debian:testing-slim as builder
RUN --mount=type=cache,target=/var/cache/apt/archives \
    apt-get update && \
    apt-get install --no-install-recommends -V -y debootstrap && \
    rm -rf /var/lib/apt/lists/*
ADD ./devuan-archive-keyring.gpg .
ADD ./debootstrap-scripts-ceres /usr/share/debootstrap/scripts/chimaera
RUN --mount=type=cache,target=/var/cache/apt/archives \
    debootstrap --variant=minbase --include=bash,iproute2,tini,curl,wget,procps,python3,python3-pip,python3-venv --keyring=./devuan-archive-keyring.gpg chimaera ./chimaera-python http://pl.deb.devuan.org/merged
WORKDIR ./chimaera-python
# Next steps decreases image size from 220MB to 92MB (see also: Ubuntu `unminimize` command).
RUN find ./var/lib/apt/lists -type f -delete; find ./var/cache/apt/archives/ -type f -delete; rm -r ./usr/share/doc ./usr/share/man
RUN find ./usr/share/locale -mindepth 1 -maxdepth 1 ! -name 'en*' ! -name 'pl*' | xargs rm -r


FROM scratch
COPY --from=builder ./chimaera-python /
CMD ["/bin/bash"]

LABEL org.opencontainers.image.description 'GNU/Linux Devuan 4 Chimaera + Python3 (pip3, venv), without man pages and documentation'
LABEL org.opencontainers.image.source 'https://gitlab.com/vazhnov/devuan-docker-images/-/blob/main/Devuan/Dockerfile_devuan_4_chimaera_python'
