Table of contents:

[[_TOC_]]

Status: early experiments.

## About

Automatically create Devuan containers from scratch by Docker.

See GitLab pipelines section for the latest builds status: https://gitlab.com/vazhnov/devuan-docker-images/-/pipelines

See GitLab container registry with images: https://gitlab.com/vazhnov/devuan-docker-images/container_registry

## Devuan

> Devuan GNU+Linux is a fork of Debian without systemd that allows users to reclaim control over their system by avoiding unnecessary entanglements and ensuring [Init Freedom](https://www.devuan.org/os/init-freedom).

In my opinion, Devuan is a Debian of healthy person. It is just a better Debian version, where you have more freedom.

### List of packages and versions

For every build, artifacts can be downloaded with output of commands `dpkg -l` and `cat /etc/os-release`.

See button "Download" near every pipeline/job run.

## How to pull the container image

Example:

```sh
sudo docker pull registry.gitlab.com/vazhnov/devuan-docker-images/devuan_5_daedalus_python:latest
```

Other images and tags can be found in https://gitlab.com/vazhnov/devuan-docker-images/container_registry .

## How to use the container image

Example:

```sh
sudo docker run registry.gitlab.com/vazhnov/devuan-docker-images/devuan_5_daedalus_python:latest cat /etc/os-release
```

should return:

```ini
PRETTY_NAME="Devuan GNU/Linux 5 (daedalus/ceres)"
NAME="Devuan GNU/Linux"
VERSION_ID="5"
VERSION="5 (daedalus/ceres)"
VERSION_CODENAME="daedalus ceres"
ID=devuan
ID_LIKE=debian
HOME_URL="https://www.devuan.org/"
SUPPORT_URL="https://devuan.org/os/community"
BUG_REPORT_URL="https://bugs.devuan.org/"
```

## Different GitLab pipelines

I couldn't find an ideal way to build images with GitLab pipelines. Here are 4 different pipelines, every of them can be used:

### .gitlab-ci\_SHELL.yaml

Link: [.gitlab-ci\_SHELL.yaml](.gitlab-ci_SHELL.yaml).

Works only if you have your own gitlab-runner with shell executor, installed to Debian-based Linux machine, see [Install GitLab Runner using the official GitLab repositories](https://docs.gitlab.com/runner/install/linux-repository.html).

The best about caching and speed.

### .gitlab-ci\_DOCKER-in-DOCKER.yaml

Link: [.gitlab-ci\_DOCKER-in-DOCKER.yaml](.gitlab-ci_DOCKER-in-DOCKER.yaml).

Works with GitLab shared runners (tag `gitlab-org-docker`). Spends your CI/CI minutes, about 5 minutes for every run. Check your limit at [Preferences → Usage quotas](https://gitlab.com/-/profile/usage_quotas).

Uses "Docker in Docker" (DIND). Very bad about caching: stages can't use local image, so they push and pull image every time. No caching at all, even between stages: [Caching for docker-in-docker builds](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/17861#note_187018544).

### .gitlab-ci\_DOCKER-in-DOCKER\_all-in-one.yaml

Link: [.gitlab-ci\_DOCKER-in-DOCKER\_all-in-one.yaml](.gitlab-ci_DOCKER-in-DOCKER_all-in-one.yaml).

Almost the same as previous one, [.gitlab-ci\_DOCKER-in-DOCKER.yaml](#gitlab-ci_docker-in-dockeryaml), but 3 stages (build-test-deploy) were squashed into one big stage, so no need in extra pushing and pulling the image. Works a little bit faster, spends about 3-4 minutes per run.

### .gitlab-ci\_DOCKER-buildah.yaml

Link: [.gitlab-ci\_DOCKER-buildah.yaml](.gitlab-ci_DOCKER-buildah.yaml).

An experiment to avoid using "Docker in Docker" complexity with no caching.

Doesn't work yet.

## How to clone and configure your project

1. Clone the repository into your GitLab account,
2. Choose runner executor to use: [Different GitLab pipelines](#different-gitlab-pipelines):
3. Change your project settings: Settings → CI/CD → General pipelines → CI/CD configuration file, type the pipeline code filename, for example `.gitlab-ci_SHELL.yaml`.
4. Add Schedules if you need, see examples in https://gitlab.com/vazhnov/devuan-docker-images/-/pipeline_schedules
5. Add auto-clean up rules: Settings → Packages and registries → Clean up image tags.
6. Set timeout in Settings → CI/CD → General pipelines → Timeout, "15 minutes" is enough (by default = 1 hour).
7. Check your account limits at [Preferences → Usage quotas](https://gitlab.com/-/profile/usage_quotas).

## TODO

* Better tests.
* Auto-update `devuan-archive-keyring.gpg` or at least warn when it was changed in upstream.
* Auto-update `debootstrap-scripts-ceres` or at least warn when it was changed in upstream.
* Use `kaniko` or `buildah` to build images, to avoid complex setup with "Docker in Docker" (see https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-alternatives ).

## Known issues

* `CI_COMMIT_TAG` is not used to tag the container images, because it represents the pipeline repository changes, which should not affect the resulting images.
* "Docker in Docker" (DIND) is not used here because of issue with testing local images: https://stackoverflow.com/questions/67574550/gitlab-build-design-tests-from-local-image

## Copyright

All files of this repository (including scripts and pipeline configuration) are distributed under MIT license, except of:
* `Devuan/debootstrap-scripts-ceres` — this file copied from `/usr/share/debootstrap/scripts/ceres` of `debootstrap` package, license: ?
* `Devuan/devuan-archive-keyring.gpg` — this file copied from `/usr/share/keyrings/devuan-archive-keyring.gpg` of `devuan-keyring` package, license: ?

## Links

* https://gitlab.com/vazhnov/devuan-docker-images — this project

### Another Devuan docker images

* https://git.devuan.org/paddy-hack/container-images ([GitLab mirror](https://gitlab.com/paddy-hack/container-images)) — is using to push images into https://hub.docker.com/r/devuan/devuan: [\[ANNOUNCE\] Devuan container images published on the Docker Hub](https://lists.dyne.org/lurker/message/20221225.063459.ed73c759.en.html).
* https://gitlab.com/paddy-hack/devuan — outdated, mostly shell scripts.
* https://github.com/dyne/docker-devuan-builds — is using for https://hub.docker.com/r/dyne/devuan.

### See also

* https://hub.docker.com/r/vazhnov/devuan — my old registry, no automation because of free account.
* https://gitlab.com/vazhnov/proxmox-devuan-containers — LXC containers for [Proxmox virtual environment](https://proxmox.com/en/).
* https://gitlab.com/vazhnov/devuan-debootstrap-grub-efi — install Devuan with `debootstrap`, configure EFI.
* https://github.com/cloux/aws-devuan — build Devuan images for AWS EC2.
